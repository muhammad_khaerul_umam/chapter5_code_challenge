const express = require('express');
const internalError = require('./internalError');
const app = express();

app.use(express.json())

let myLogger = function (request, response, next) {
    console.log('LOGGED')
    next()
}
app.use(myLogger)

app.set ('view engine', 'ejs')
app.use(express.static(__dirname + '/public'));

app.use(internalError);


app.get('/', function(request,response){
    response.render('index')
})

app.get('/play', function (request, response){
    response.render('play')
})



app.listen(3000, function(){
    console.log('Server Hidup')
})

